import transformers
import evaluate
import accelerate

import json
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns

sns.set(font_scale=1.2, palette="Set2")

import torch
import torch.nn as nn
import torch.nn.functional as F
import sentencepiece
import gc

from torch.utils.data import DataLoader
from datasets import Dataset, load_dataset

from transformers import (
    AutoTokenizer,
    AutoModel,
    DataCollatorWithPadding,
    DataCollatorForSeq2Seq,
    AutoModelForSequenceClassification,
    AdamW,
    TrainingArguments,
    Trainer,
    default_data_collator,
    MBartForConditionalGeneration,
    MBart50Tokenizer
)

from peft import (
    get_peft_config,
    get_peft_model,
    get_peft_model_state_dict,
    TaskType,
    LoraConfig
)

from tqdm.auto import tqdm
from pathlib import Path

from sklearn.model_selection import train_test_split


class ModelMBart:
    def __init__(self, epochs=1, r=16, fine_tuned=False, path="./model"):
        self.references = None
        self.device = "cuda:0" if torch.cuda.is_available() else "cpu"
        self.dataset = load_dataset('IlyaGusev/gazeta', revision="v2.0")
        self.tokenizer = AutoTokenizer.from_pretrained('facebook/mbart-large-cc25', legacy=True)
        self.data_collator = DataCollatorForSeq2Seq(self.tokenizer, return_tensors="pt")
        self.path = path
        self.tokenizing()

        if fine_tuned:
            print("Fine tuned model init")
            self.model = MBartForConditionalGeneration.from_pretrained(self.path)
        else:
            print("Pretrained model init")
            self.model = MBartForConditionalGeneration.from_pretrained('facebook/mbart-large-cc25')
            target_modules = [name for name, layer in self.model.named_modules() if isinstance(layer, nn.Linear)]
            peft_config = LoraConfig(
                task_type=TaskType.SEQ_2_SEQ_LM,
                inference_mode=False,
                r=r,
                lora_alpha=64,
                lora_dropout=0.1,
                target_modules=target_modules
            )
            self.model = get_peft_model(self.model, peft_config)
            self.args = TrainingArguments(
                f"newMBart{r}",
                overwrite_output_dir=True,
                logging_steps=100,
                learning_rate=5e-4,
                per_device_train_batch_size=16,
                num_train_epochs=epochs,
                weight_decay=0.01,
                report_to="none",
            )

        self.model = self.model.to(self.device)

    def preprocess_function(self, examples):
        """ Preprocesing """

        inputs = examples["text"]
        targets = examples["summary"]
        model_inputs = self.tokenizer(inputs,
                                      max_length=512,
                                      truncation=True)
        labels = self.tokenizer(targets,
                                max_length=128,
                                truncation=True)
        model_inputs["labels"] = labels["input_ids"]
        return model_inputs

    def tokenizing(self):
        self.dataset["train"] = self.dataset["train"].map(
            self.preprocess_function,
            num_proc=6,
            desc="Running tokenizer on train dataset...",
        )

        self.dataset["validation"] = self.dataset["validation"].map(
            self.preprocess_function,
            num_proc=6,
            desc="Running tokenizer on validation dataset...",
        )
        unused_columns = ["text", "summary", "title", "date", "url"]
        self.dataset["train"] = self.dataset["train"].remove_columns(unused_columns)

        self.references = self.dataset["validation"]["summary"]
        self.dataset["validation"] = self.dataset["validation"].remove_columns(unused_columns + ["labels"])

    def train(self):
        print("Started!")
        trainer = Trainer(
            self.model,  # model
            self.args,  # training arguments
            train_dataset=self.dataset["train"],  # training dataset
            data_collator=self.data_collator,  # data collator
        )

        trainer.train()
        trainer.save_model(self.path)
        del trainer

    def evaluate(self):
        val_dataloader = DataLoader(self.dataset["validation"],
                                    batch_size=16,
                                    collate_fn=DataCollatorWithPadding(self.tokenizer),
                                    shuffle=False)
        predictions = []
        print("Evaluation started")
        for batch in tqdm(val_dataloader):
            output_ids = self.model.generate(
                input_ids=batch["input_ids"].cuda(),
                max_new_tokens=128,
                num_beams=4,  # beam_size
                no_repeat_ngram_size=4,  # the prohibition of repetition n-gram
                early_stopping=True
            )
            predictions.extend(self.tokenizer.batch_decode(output_ids, skip_special_tokens=True))
        
        print("Metrics computation started")
        rouge = evaluate.load('rouge')
        results = rouge.compute(predictions=predictions,
                                references=self.references,
                                tokenizer=lambda x: x.split())

        print("Evaluation is done")
        print(str(results))


for r in range(0, 8):
    model = ModelMBart(r=2**r, epochs=7, path=f"new_finalMBart{2**r}")
    model.train()
    model.evaluate()
    del model
    gc.collect()
    torch.cuda.empty_cache()

